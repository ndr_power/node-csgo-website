/** ------Node-Modules------ **/

var SteamCommunity = require('steamcommunity');
var SteamTotp = require('steam-totp');
var steamtrade = require('steam-tradeoffers');
var config = require('./../config/bot.config.js');
var SCommunity = new SteamCommunity();
var trades = new steamtrade();
var twoFCode = SteamTotp.generateAuthCode(config.bot.sharedSecret);
var acc_inf4log = {
    "accountName": config.bot.accountName,
    "password": config.bot.password,
    "twoFactorCode": twoFCode
};

/** ----End of Node-Modules---- */

SCommunity.login(acc_inf4log, login );

/**  Steam-Community Logging in, Confirmations, Functions   **/
SCommunity.on('confKeyNeeded', function(tag, callback) {
    callback(null, time, SteamTotp.getConfirmationKey(account.identity_secret, time(), tag));
});

SCommunity.on('newConfirmation', function(confirmation) {
    var time = time();
    var key = SteamTotp.getConfirmationKey(account.identity_secret, time, 'allow');
    confirmation.respond(time, key, true, function(err) {
        if(err) {
            return;
        }
    });
});

function login(err, _sessionID, _cookies, _steamGuard){
    if (err) {
        if (err.message = 'SteamGuardMobile'){
            twoFCode = SteamTotp.generateAuthCode(config.bot.sharedSecret);
            setTimeout(function () {
                SCommunity.login(acc_inf4log, login);
            }, 5000);
            return;
        }
        leave();
    }
    log('Successfully started!')
    config.sessionID = _sessionID;
    config.cookies = _cookies;
    SCommunity.getWebApiKey('ndrstudio.ru', getApiKey)
    SCommunity.startConfirmationChecker(10000, config.bot.identitySecret);
}

module.exports.sendTrade = function(_assetids, _partner, _token, _price, callback){
    var assetids = _assetids;
    var partner = _partner;
    var token = _token;
    //var myItmes = req.myItems;
    //var hisItems = req.hisItems;
    var sendItemsFromHim = [];
    //var steamid = req.steamid;
    for(var i = 0; i<assetids.length;i++){
        //if(assetids[i] = "") continue;
        sendItemsFromHim.push({
            appid: 578080,
            amount: 1,
            contextid: 2,
            assetid: assetids[i].assetid
        })
    }
    console.log(sendItemsFromHim)
    trades.makeOffer({
        partnerAccountId: partner,
        accessToken: token,
        itemsFromThem:sendItemsFromHim,
        itemsFromMe: [],
        message: 'Skins selected for $' + _price
    }, function(err, r){
        if(err){
            log(`err sending ${err}`)
            callback(true, err);
        }
        else{
          log('sent offer')
            callback(false, r)
        }
    });
}
function offersSetUp(){
    log('offers set up')
    trades.setup({
        sessionID: config.sessionID,
        webCookie: config.cookies,
        APIKey: config.apikey
    })
}
function getApiKey(err, key){
    if(err) {
        leave();
    }else{
        config.apikey = key;
        offersSetUp();
        SCommunity.loggedIn(checkLogged);
        //log(config.apikey);
    }
}
function checkLogged(err, loggedIn, familyView){
    if(err || !loggedIn){
        leave();
    }else{
        config.auth = true;
    }
}
function leave(){
    process.exit(0);
}
function log(_msg){
   console.log(`(BOT) ${_msg}`)
}
