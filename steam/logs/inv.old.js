var SteamUser = require('steam-user');
var client = new SteamUser();
var TradeOfferManager = require('steam-tradeoffer-manager')
const fs = require('fs')
var request = require('request');
var OPSkinsAPI = require('@opskins/api');
var opskins = new OPSkinsAPI('e49235fa53e336ee7278059693cf4a');
var manager = new TradeOfferManager({
    "steam": client,
    "domain": "ndrstudio.ru", // Fill this in with your own domain
    "language": "en"
});


fs.readFile(__dirname + '/cache/prices.txt', (err, data)=>{
  if(!err)
  prices = JSON.parse(data);
})
fs.readFile(__dirname + '/cache/prices_pubg.txt', (err, data)=>{
  if(!err)
  prices_pubg = JSON.parse(data);
})
fs.readFile(__dirname + '/cache/prices_dota2.txt', (err, data)=>{
  if(!err)
  prices_dota2 = JSON.parse(data);
})
function updatePrices(){
  opskins.getLowestPrices(578080, (err, prices) => {
    if(!err){
      fs.writeFileSync(__dirname + '/cache/prices_pubg.txt', JSON.stringify(prices), ()=>{	if(err) throw err;})
      var prices_pubg = prices;
    }
  });
  request('https://api.csgofast.com/price/all', (err, res, body)=> {
    if(!err) {
    fs.writeFileSync(__dirname + '/cache/prices.txt', body, ()=>{	if(err) throw err;})
    }
      prices_csgo = JSON.parse(body)
})
//pubg


//dota2
  opskins.getLowestPrices(570, (err, prices2) => {
    if(!err){
      fs.writeFileSync(__dirname + '/cache/prices_dota2.txt', JSON.stringify(prices2), ()=>{	if(err) throw err;})
      var prices_dota2 = prices2;
    }
  })


}
updatePrices();
setInterval(()=>{
  updatePrices()
}, 60*60*1000);


module.exports.getUserInv = function(_steamid, callback) {
  steamid = _steamid.toString();
  manager.getUserInventoryContents(_steamid.toString(), 730, 2, true, (err, inv, currencies) =>{
  var inventory_csgo = [];
  if(!err){
    for(let i = 0; i<inv.length; i++){
      if(inv[i].descriptions[0].value == ' '){
      inventory_csgo.push({
        assetid: inv[i].assetid,
        exterior: false,
        market_hash_name: inv[i].market_hash_name,
        icon_url: 'https://steamcommunity-a.akamaihd.net/economy/image/' + inv[i].icon_url, /* https://steamcommunity-a.akamaihd.net/economy/image/ */
        color: 'background-color: #'+inv[i].name_color,
        name: inv[i].name,
        price: prices[inv[i].market_hash_name]
      })
    }else{
      inventory_csgo.push({
        assetid: inv[i].assetid,
        exterior: inv[i].descriptions[0].value,
        market_hash_name: inv[i].market_hash_name,
        icon_url: 'https://steamcommunity-a.akamaihd.net/economy/image/' + inv[i].icon_url, /* https://steamcommunity-a.akamaihd.net/economy/image/ */
        color: 'background-color: #'+inv[i].name_color,
        name: inv[i].name,
        price: prices[inv[i].market_hash_name]
      })
    }
    }
    //DOTA2
    manager.getUserInventoryContents(_steamid.toString(), 570, 2, true, (err2, inv2, currencies2) =>{
    var inventory_dota = [];
    if(!err2){
      for(let i = 0; i<inv2.length; i++){
        if(inv2[i].descriptions[0].value == ' '){
        inventory_dota.push({
          assetid: inv2[i].assetid,
          exterior: false,
          market_hash_name: inv[i].market_hash_name,
          icon_url: 'https://steamcommunity-a.akamaihd.net/economy/image/' + inv[i].icon_url, /* https://steamcommunity-a.akamaihd.net/economy/image/ */
          color: 'background-color: #'+inv[i].name_color,
          name: inv2[i].name,
          price: prices_dota2[inv[i].market_hash_name].price/100
        })
      }else{
        inventory_dota.push({
          assetid: inv2[i].assetid,
          exterior: inv2[i].descriptions[0].value,
          market_hash_name: inv2[i].market_hash_name,
          icon_url: 'https://steamcommunity-a.akamaihd.net/economy/image/' + inv2[i].icon_url, /* https://steamcommunity-a.akamaihd.net/economy/image/ */
          color: 'background-color: #'+inv2[i].name_color,
          name: inv2[i].name,
          price: prices_dota2[inv2[i].market_hash_name].price/100
        })
      }
      }
        //PUBG
        manager.getUserInventoryContents(_steamid.toString(), 578080, 2, true, (err3, inv3, currencies3) =>{
        var inventory_pubg = [];
        if(!err2){
          for(let i = 0; i<inv3.length; i++){

            inventory_pubg.push({
              assetid: inv3[i].assetid,
              exterior: false,
              market_hash_name: inv3[i].market_hash_name,
              icon_url: 'https://steamcommunity-a.akamaihd.net/economy/image/' + inv3[i].icon_url, /* https://steamcommunity-a.akamaihd.net/economy/image/ */
              color: 'background-color: #'+inv3[i].name_color,
              name: inv3[i].name,
              price: prices_pubg[inv3[i].market_hash_name].price/100
            })

          }
          console.log(inventory_csgo)
          callback(inventory_csgo, inventory_dota, inventory_pubg);
        }else
          callback(err3)
        });
    }else
      callback(err2)
    });
  }else
    callback(err)
});

// Dota2

}

//PUBG
