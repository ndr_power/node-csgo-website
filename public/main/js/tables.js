//Propeller Customised Javascript code
$(document).ready(function() {
var table = $('#example-checkbox').DataTable({
  responsive: {
    details: {
      type: 'column',
      target: 'tr'
    }
  },
    columnDefs: [ {
    orderable: false,
    className: 'select-checkbox',
    targets:0,
    } ],
  select: {
    style: 'multi',
    selector: 'td:first-child'
  },
  order: [ 1, 'asc' ],
  bFilter: true,
  bLengthChange: true,
  pagingType: "simple",

   paging: true,
   "searching": true,
  "language": {
    "info": " _START_ - _END_ of _TOTAL_ ",
    "sLengthMenu": "<span class='custom-select-title'>Rows per page:</span> <span class='custom-select'> _MENU_ </span>",
    "sSearch": "",
    "sSearchPlaceholder": "Search",
    "paginate": {
      "sNext": " ",
      "sPrevious": " "
    },
  },
  dom:
    "<'pmd-card-title'<'data-table-title'><'search-paper pmd-textfield'f>>" +
    "<'custom-select-info'<'custom-select-item'><'custom-select-action'>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'pmd-card-footer' <'pmd-datatable-pagination' l i p>>",
});

$('.custom-select-info').hide();
})
