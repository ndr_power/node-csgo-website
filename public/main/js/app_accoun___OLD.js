//Propeller Customised Javascript code
$(document).ready(function() {
var table = $('#example-checkbox').DataTable({
  responsive: {
    details: {
      type: 'column',
      target: 'tr'
    }
  },

  select: {
    style: 'multi',
    selector: 'td:first-child'
  },
  order: [ 1, 'asc' ],
  bFilter: true,
  bLengthChange: true,
  pagingType: "simple",

   paging: true,
   "searching": true,
  "language": {
    "info": " _START_ - _END_ of _TOTAL_ ",
    "sLengthMenu": "<span class='custom-select-title'>Rows per page:</span> <span class='custom-select'> _MENU_ </span>",
    "sSearch": "",
    "sSearchPlaceholder": "Search",
    "paginate": {
      "sNext": " ",
      "sPrevious": " "
    },
  },
  dom:
    "<'pmd-card-title'<'data-table-title'><'search-paper pmd-textfield'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'pmd-card-footer' <'pmd-datatable-pagination' l i p>>",
});

$('.custom-select-info').hide();
 // end of something click
	$('#savelink').click(function(){
		$.ajax({
			url: '/save_tlink',
			type: 'GET',
			data: {data: $( "#link_input" ).val()},
			success: function(msg){
				console.log(msg);
				if(msg.success){

					notie.alert({
				  type: 1,
					text: `Successfully saved Trade Link!`,
				  stay: false, // optional, default = false
				  time: 3,
				  position: 'top'
				})
				}

			}
		})

	})
})
