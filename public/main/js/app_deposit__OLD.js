//Propeller Customised Javascript code
$(document).ready(function() {
var table = $('#example-checkbox').DataTable({
  responsive: {
    details: {
      type: 'column',
      target: 'tr'
    }
  },
    columnDefs: [ {
    orderable: false,
    className: 'select-checkbox',
    targets:0,
    } ],
  select: {
    style: 'multi',
    selector: 'td:first-child'
  },
  order: [ 1, 'asc' ],
  bFilter: true,
  bLengthChange: true,
  pagingType: "simple",

   paging: true,
   "searching": true,
  "language": {
    "info": " _START_ - _END_ of _TOTAL_ ",
    "sLengthMenu": "<span class='custom-select-title'>Rows per page:</span> <span class='custom-select'> _MENU_ </span>",
    "sSearch": "",
    "sSearchPlaceholder": "Search",
    "paginate": {
      "sNext": " ",
      "sPrevious": " "
    },
  },
  dom:
    "<'pmd-card-title'<'data-table-title'><'search-paper pmd-textfield'f>>" +
    "<'custom-select-info'<'custom-select-item'><'custom-select-action'>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'pmd-card-footer' <'pmd-datatable-pagination' l i p>>",
});

$('.custom-select-info').hide();



	$('#something').click(function(){
		var sum = 0;
var assetids = [];
for(let i = 0;i<table.rows('.selected').data().length;i++) {
		assetids.push(table.rows('.selected').data()[i][3]);
		sum += parseFloat(table.rows('.selected').data()[i][5]);
}
	$.ajax({
		url:'/deposit_back',
		type: 'GET',
		data: {data: assetids, sum: sum},
		success: function(msg){
			if(msg.success){
			console.log(msg.info)
			notie.alert({
			type: 1,
			text: `Successfully sent you an offer. TID: #${msg.info.tradeofferid}`,
			stay: false, // optional, default = false
			time: 3,
			position: 'top'
		})
	}else{
		notie.alert({
		type: 3,
		text: `Error: ${msg.info}`,
		stay: false, // optional, default = false
		time:5,
		position: 'top'
	})
	}
		}
	})

		console.log(`sum: ${sum}, assetids: ${assetids}`)

	}); // end of something click
	$('#savelink').click(function(){
		$.ajax({
			url: '/save_tlink',
			type: 'GET',
			data: {data: $( "#link_input" ).val()},
			success: function(msg){
				console.log(msg);
				if(msg.success){

					notie.alert({
				  type: 1,
					text: `Successfully saved Trade Link!`,
				  stay: false, // optional, default = false
				  time: 3,
				  position: 'top'
				})
				}

			}
		})

	})
})
